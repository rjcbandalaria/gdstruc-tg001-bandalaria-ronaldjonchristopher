#include<iostream>
#include<string> 

using namespace std; 

void inputGuildName(string& guildName) {
	cout << "Enter your Guild Name" << endl;
	cin >> guildName; 
}

void displayGuildMember(string* members, int size) {

	for (int i = 0; i < size; i++) {
		if (members[i] != "") {
			cout << "Member " << i + 1 << ": " << members[i] << endl;
		}
		else if (members[i] == "") {
			cout << "[Empty Slot]" << endl; 
		}
	}
}

void inputNumberofMember(int& guildSize) {
	cout << "How many members? " << endl;
	cin >> guildSize;
}

void inputMemberName(string* members, int size) {
	string userName;
	for (int i = 0; i < size; i++) {
		if (members[i] == "") {
			cout << "Enter Member " << i + 1 << " username: ";
			cin >> userName;
			members[i] = userName;
		}
	}
}

void renameMember(string* member) {
	int memberLocation; 
	string newUserName;
	cout << "Which member do you want to rename? "; 
	cin >> memberLocation; 
	cout << endl;
	cout << "You are renaming username: " << member[memberLocation-1] << endl; 
	cout << "What is your new Username? " << endl;
	cout << "Username: "; 
	cin >> newUserName; 

	member[memberLocation - 1] = newUserName;
}

void increaseGuildSize(int& size){
	int newSize;
	cout << "How many members to add? ";
	cin >> newSize;
	size = size + newSize;
}

void transferMembers(int size, string* members, string *tempStorage) {
	for (int i = 0; i < size; i++) {
		*(tempStorage+i) = *(members+i);
	}
}

void removeGuildMember (int &size, string* members, string* tempStorage) {
	int userChoice;
	cout << "Choose a member to be removed " << endl; 
	cin >> userChoice;
	userChoice--;
	int tempArrayAccess = 0;
	for (int i = 0; i < size; i++) {
		if (i != userChoice) {
			tempStorage[tempArrayAccess] = members[i];
			tempArrayAccess++;
		}
	}
}
void reduceGuildSize(int& size) {
	size--;
}
int main() {

	string guildName;
	// Input the name of the Guild
	inputGuildName(guildName);
	// Initialize number of members 
	int guildSize; 
	inputNumberofMember(guildSize);
	string *guildMembers = new string[guildSize]; 
	// Input member names 
	inputMemberName(guildMembers, guildSize);
	system("cls");
	// Menu Loop

	while (true) {
		int userChoice;
		cout << "Choose next action " << endl;
		cout << "[1] - Rename member" << endl;
		cout << "[2] - Add new members " << endl;
		cout << "[3] - Delete members " << endl;
		cout << "[4] - Display members" << endl;
		cout << "[5] - Exit Program" << endl;
		cin >> userChoice;
		if (userChoice == 1) {
			// Rename Member Username 
			displayGuildMember(guildMembers, guildSize);
			renameMember(guildMembers);
		}
		else if (userChoice == 2) {
			//Add new members 
			int tempGuildSize = guildSize;
			string* tempMemberStorage = new string[tempGuildSize];
			transferMembers(tempGuildSize, guildMembers, tempMemberStorage);
			delete[] guildMembers;
			increaseGuildSize(guildSize);
			guildMembers = new string[guildSize];
			transferMembers(tempGuildSize, tempMemberStorage, guildMembers);
			delete[] tempMemberStorage;
			inputMemberName(guildMembers, guildSize);
		}
		else if (userChoice == 3) {
			// Remove member
			int tempGuildSize = guildSize; // intitialize temp size 
			string* tempMemberStorage = new string[tempGuildSize]; // initialize temp storage 
			// Removing member
			removeGuildMember(guildSize, guildMembers, tempMemberStorage); // chose and transfer of members to temp 
			delete[] guildMembers;
			// reducing size 
			reduceGuildSize(guildSize);
			guildMembers = new string[guildSize];
			transferMembers(tempGuildSize, tempMemberStorage, guildMembers);
			delete[] tempMemberStorage;
			cout << "Remaining Members: " << endl;
			displayGuildMember(guildMembers, guildSize);
		}
		else if (userChoice == 4) {
			cout << guildName << endl;
			displayGuildMember(guildMembers, guildSize);
		}
		else if (userChoice == 5) {
			break;
		}
		system("pause");
		system("cls");
	}
	delete[] guildMembers;
	system("pause");
}