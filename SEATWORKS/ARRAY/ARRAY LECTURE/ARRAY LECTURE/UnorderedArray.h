#pragma once
#include<assert.h>
#include<iostream>
#include<string>

using namespace std;

template<class T>
class UnorderedArray {

public:

	UnorderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0) // initialization list 
	{
		if (size) {
			mMaxSize = size;

			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize); // allocates memory based on the bytes of T 

			mGrowSize = ((growBy > 0) ? growBy : 0); // ternary operator
			// single line if else statement
		}
	}

	virtual ~UnorderedArray() {
		if (mArray != NULL) {
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value) {
		assert(mArray != NULL); // assert if the condition is true, if false, it will not push through

		if (mNumElements >= mMaxSize) {
			expand();
		}

		mArray[mNumElements] = value;
		mNumElements++;

	}

	virtual T& operator[] (int index) { // can use the bracket operator with an index inside 

		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];

	}

	int getSize() {
		return mNumElements;
	}

	virtual void pop() {

		if (mNumElements > 0) {
			mNumElements--;
		}
	}

	virtual void remove(int index) {
		assert(mArray != NULL);

		if (index >= mMaxSize) {// index is more than the current size
			return;
		}
		for (int i = index; i < mMaxSize - 1; i++) {
			mArray[i] = mArray[i + 1];


		}
		mMaxSize--;
		if (mNumElements >= mMaxSize) {
			mNumElements = mMaxSize;
		}
	}	

	virtual int linearSearch() {
		int numberLocator;

		cout << "Search a number" << endl;
		cout << "Search for number: ";
		cin >> numberLocator;

		for (int i = 0; i < mNumElements; i++) {
			if (numberLocator == mArray[i]) {
				return i;
			}
		}
		return -1;
	}
private:
	T* mArray; // Type T
	int mMaxSize;
	int mGrowSize;
	int mNumElements;

	bool expand() { // call if array is full 
		if (mGrowSize <= 0) {
			return false;
		}
		T* temp = new T[mMaxSize + mGrowSize];

		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;

		return true;
	}
};