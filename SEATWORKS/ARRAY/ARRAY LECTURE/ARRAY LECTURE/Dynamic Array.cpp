#include<iostream>
#include<string> 
#include"UnorderedArray.h"
#include<time.h>

using namespace std; 

void inputSizeOfArray(int &size) {

	int userChoice;
	cout << "Input size of Array" << endl; 
	cout << "Size: ";
	cin >> size;
}

int linearSearch(UnorderedArray<int> array) {
	int numberLocator; 
	cout << "Search a number" << endl;
	cout << "Search for number: ";
	cin >> numberLocator;

	for (int i = 0; i < array.getSize(); i++) {
		if (numberLocator == array[i]) {
			return i;
		}

	}

	return -1;
}

void main() {
	// unordered array 
	// ordered array 
	// part of Midterms exam 

	// Vector is an array 
	// Accessing it is linear 
	
	// Create own vector 
	srand(time(NULL));
	int arraySize = 0;
	int removeNumber;
	int numberLocator;
	// #1 Unordered Array based on the size that the user wants 
	inputSizeOfArray(arraySize);

	UnorderedArray<int> testArray(arraySize);
	
	//Fill Unordered Array with Random Values 
	for (int i = 0; i < arraySize; i++) {
		testArray.push((rand() % 100)+1);
	}
	// Display Array 
	for (int i = 0; i < testArray.getSize(); i++) {
		cout << testArray[i] << endl;
	}
	// Remove element of choice
	cout << "Select the position of the number to be removed from the list" << endl;
	cout << "Number: ";
	cin >> removeNumber;

	testArray.remove(removeNumber - 1);

	for (int i = 0; i < testArray.getSize(); i++) {
		cout << testArray[i] << endl;
	}

	// Linear Search function 
	numberLocator = testArray.linearSearch();

	if (numberLocator != -1) {
		cout << "Value Found!" << endl;
		cout << "Value: " << testArray[numberLocator] << endl;
	}
	else {
		cout << "Value not found" << endl;
	}
	

	system("pause");
		
}