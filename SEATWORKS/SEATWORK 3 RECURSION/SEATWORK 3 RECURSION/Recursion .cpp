#include<iostream>

using namespace std; 

void computeSumOfDigits(int number, int& sumOfDigits) {
	if (number <= 0) {
		return;
	}
	sumOfDigits +=(number % 10);
	number = number / 10;
	computeSumOfDigits(number, sumOfDigits);
}

void main() {
	int sumOfDigits = 0;
	computeSumOfDigits(159, sumOfDigits);
	cout << "Sum of Digits: " << sumOfDigits << endl;
	system("pause");

}