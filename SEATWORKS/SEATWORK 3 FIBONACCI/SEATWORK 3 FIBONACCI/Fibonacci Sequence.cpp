#include <iostream>
#include<string>

using namespace std;

void printFibonnaci(int numberOfDigits, int firstNumber, int secondNum) {
	if (numberOfDigits <= 0) {
		return;
	}
	int sum; 
	sum = firstNumber + secondNum;
	cout << sum << " "; 
	secondNum = firstNumber;
	firstNumber = sum; 
	printFibonnaci(numberOfDigits - 1, firstNumber, secondNum);
}

int inputNumberOfTerms() {
	int userInput;
	cout << "Fibonacci Sequence" << endl; 
	cout << "Enter number of terms to be displayed" << endl;
	cin >> userInput;

	if (userInput > 0) {
		return userInput;
	}
	else {
		inputNumberOfTerms();
	}
}

void main() {
	int numberOfTerms = inputNumberOfTerms();
	int firstNum = 0;
	int secondNum = 1;
	printFibonnaci(numberOfTerms, firstNum, secondNum);
	system("pause");
}