#include<iostream>

using namespace std; 

int inputNumber() {
	int userChoice;
	cout << "Enter number: "; 
	cin >> userChoice;
	if (userChoice < 0) {
		inputNumber();
	}
	return userChoice;
}
void checkPrimeNumber(int number, int factor = 2) {
	if (factor >= number) {
		cout << "The number is a Prime Number" << endl;
		return;
	}
	if (number % factor == 0) {
		cout << "The number is not a Prime Number" << endl;
		return;
	}
	checkPrimeNumber(number, factor + 1);
}

void main() {
	int number = inputNumber();
	checkPrimeNumber(number);
	system("pause");
}