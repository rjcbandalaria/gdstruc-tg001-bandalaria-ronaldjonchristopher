#include <iostream>
#include <string>
#include "UnorderedArray.h"
#include "OrderedArray.h"
#include <time.h>

using namespace std;



void main()
{
	srand(time(NULL));
	int userChoice;
	int input;
	cout << "Enter size of array: ";
	int size;
	cin >> size;
	system("cls");
	UnorderedArray<int> unordered(size);
	OrderedArray<int> ordered(size);

	for (int i = 0; i < size; i++)
	{
		int rng = rand() % 101;
		unordered.push(rng);
		ordered.push(rng);
	}
	while (true) {
		cout << "\nGenerated array: " << endl;
		cout << "Unordered: ";
		for (int i = 0; i < unordered.getSize(); i++)
			cout << unordered[i] << "   ";
		cout << "\nOrdered: ";
		for (int i = 0; i < ordered.getSize(); i++)
			cout << ordered[i] << "   ";
		cout << endl;
		cout << "Choose next action " << endl;
		cout << "[1] - Remove number from array" << endl;
		cout << "[2] - Search number" << endl;
		cout << "[3] - Expand Array" << endl;
		cout << "[4] - Exit program" << endl;
		cin >> userChoice;

		if (userChoice == 1) {
			// Delete Element
			cout << "Delete Element at: ";
			while (true) {
				cin >> input ;
				if (input <= unordered.getSize() && input <= ordered.getSize())
				{
					break;
				}
				else {
					cout << "Number is higher than the size of the array" << endl;
				}
			}
			unordered.remove(input - 1);
			ordered.remove(input - 1);
			cout << "New Array" << endl;
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
			cout << endl; 
		}
		else if (userChoice == 2) {
			//Search 
			cout << "\n\nEnter number to search: ";
			cin >> input;
			cout << endl;
			cout << "Unordered Array(Linear Search):\n";
			int result = unordered.linearSearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;

			cout << "Ordered Array(Binary Search):\n";
			result = ordered.binarySearch(input);
			if (result >= 0)
				cout << input << " was found at index " << result << ".\n";
			else
				cout << input << " not found." << endl;
		}
		// Expand 
		else if (userChoice == 3) {
			cout << "Expand the array by: ";
			cin >> input;

			for (int i = 0; i < input; i++) {
				int randomNum;
				randomNum = rand() % 101;
				unordered.push(randomNum);
				ordered.push(randomNum);
			}
			cout << "Unordered: ";
			for (int i = 0; i < unordered.getSize(); i++)
				cout << unordered[i] << "   ";
			cout << "\nOrdered: ";
			for (int i = 0; i < ordered.getSize(); i++)
				cout << ordered[i] << "   ";
			cout << endl;
		}
		else if (userChoice == 4) {
			// Exit program
			cout << "Exiting program" << endl;
			break;
		}
		else {
			cout << "Invalid option" << endl;
		}
		system("pause");
		system("cls");
	}

	system("pause");
}