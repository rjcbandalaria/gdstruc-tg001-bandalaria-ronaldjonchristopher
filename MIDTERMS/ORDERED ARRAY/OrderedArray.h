#pragma once
#include <assert.h>
#include<iostream>
#include <string>


template<class T>
class OrderedArray
{
public:
	OrderedArray(int size, int growBy = 1) : mArray(NULL), mMaxSize(0), mGrowSize(0), mNumElements(0)
	{
		if (size)
		{
			mMaxSize = size;
			mArray = new T[mMaxSize];
			memset(mArray, 0, sizeof(T) * mMaxSize);
			mGrowSize = ((growBy > 0) ? growBy : 0);
		}
	}

	virtual ~OrderedArray()
	{
		if (mArray != NULL)
		{
			delete[] mArray;
			mArray = NULL;
		}
	}

	virtual void push(T value)
	{
		assert(mArray != NULL);

		if (mNumElements >= mMaxSize)
			expand();

		//your code goes after this line
		if (mNumElements != 0) {
			for (int i = 0; i < mNumElements; i++) {
				if (value <= mArray[i]) {
					for (int j = mNumElements; j >= i; j--) {
						mArray[j] = mArray[(j - 1)];
					}
					mArray[i] = value;
					break;
				}
			}
			if (value > mArray[mNumElements]) {
				mArray[mNumElements] = value;
			}
		}
		else if (mNumElements <= 0) {
			mArray[mNumElements] = value;
		}
		mNumElements++;
	}


	virtual void pop()
	{
		if (mNumElements > 0)
			mNumElements--;
	}

	virtual void remove(int index)
	{
		assert(mArray != NULL && index <= mMaxSize);

		for (int i = index; i < mMaxSize - 1; i++)
			mArray[i] = mArray[i + 1];

		mMaxSize--;

		if (mNumElements >= mMaxSize)
			mNumElements = mMaxSize;
	}

	virtual T & operator[](int index)
	{
		assert(mArray != NULL && index <= mNumElements);
		return mArray[index];
	}

	virtual int getSize()
	{
		return mNumElements;
	}

	virtual int binarySearch(T val)
	{
		//your code goes after this line
		T firstElement = 0;
		T lastElement = mNumElements - 1;
		T midPoint= (firstElement + lastElement) / 2;
		while (firstElement <= lastElement && firstElement >=0) {

			if (mArray[midPoint] == val) {
				return midPoint;
			}
			if (mArray[midPoint] < val) {
				firstElement = midPoint + 1;
			}
			else if (mArray[midPoint] > val){
				lastElement = midPoint;
				firstElement = midPoint - 1;
			}
			midPoint = (firstElement + lastElement) / 2;
		}
		
		return -1;
	}

private:
	T * mArray;
	int mGrowSize;
	int mMaxSize;
	int mNumElements;

	bool expand()
	{
		if (mGrowSize <= 0)
			return false;

		T * temp = new T[mMaxSize + mGrowSize];
		assert(temp != NULL);

		memcpy(temp, mArray, sizeof(T) * mMaxSize);

		delete[] mArray;
		mArray = temp;

		mMaxSize += mGrowSize;
		return true;
	}

};