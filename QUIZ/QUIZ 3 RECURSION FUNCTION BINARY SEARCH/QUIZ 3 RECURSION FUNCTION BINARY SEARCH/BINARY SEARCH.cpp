#include<iostream>
#include<string>
#include<vector>

using namespace std; 

int searchNumberBinarySearch(vector<int> numbers,int locateNumber, int firstElement, int lastElement) {
	int midPoint = (firstElement + lastElement) / 2;
	if (firstElement >= 0 && lastElement <= numbers.size()) {
		if (numbers[midPoint] == locateNumber) {
			return numbers[midPoint];
		}
		else if (numbers[midPoint] < locateNumber) {
			searchNumberBinarySearch(numbers, locateNumber, midPoint, lastElement);
		}
		else if (numbers[midPoint] > locateNumber) {
			searchNumberBinarySearch(numbers, locateNumber, firstElement, midPoint);
		}
	}
	return -1;
}
int enterNumberOfDigits() {
	int number;
	cout << "How many numbers? ";
	cin >> number;
	return number;
}
void enterNumbers(vector<int> &numbers, int numberOfDigits) {
	if (numberOfDigits > 0) {
		int number;
		cout << "Enter number: ";
		cin >> number;
		numbers.push_back(number);
		enterNumbers(numbers, numberOfDigits - 1);
	}
}
void enterNumberToLocate(int& numberToLocate) {
	cout << "Search for number: ";
	cin >> numberToLocate;
}
void arrangeOrderOfArray(vector<int> &unorderedNumbers) {
	int tempNumber;
	for (int i = 0; i < unorderedNumbers.size(); i++) {
		for (int j = i + 1; j < unorderedNumbers.size(); j++){
			if (unorderedNumbers[i] > unorderedNumbers[j]) {
				tempNumber = unorderedNumbers[i];
				unorderedNumbers[i] = unorderedNumbers[j];
				unorderedNumbers[j] = tempNumber;
			}
		}
	}
}

void main() {
	int numberOfDigits = enterNumberOfDigits();
	vector<int> numbers;
	int locateNumber;
	enterNumbers(numbers, numberOfDigits);
	arrangeOrderOfArray(numbers);
	cout << "Ordered Array: ";
	for (int i = 0; i < numbers.size(); i++) {
		cout << numbers[i] << " , ";
	}
	cout << endl;
	enterNumberToLocate(locateNumber);
	int firstElement = 0;
	int lastElement = numbers.size() - 1;
	int result = searchNumberBinarySearch(numbers, locateNumber, firstElement, lastElement);
	if (result < 0) {
		cout << "No number exists" << endl;
	}
	else if (result >= 0){
		cout << "Number " << locateNumber << " exists" << endl;
	}
	system("pause");
}