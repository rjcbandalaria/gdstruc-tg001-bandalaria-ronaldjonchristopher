#include<iostream>
#include<string>

using namespace std;

void printReverseWord(string word) {

	if (word.size() <= 1) {
		cout << word.at(0);
		return;
	}
	cout << word.at(word.size() - 1);
	printReverseWord(word.substr(0, word.size() - 1));
}
void inputWord(string& word) {
	cout << "Enter a word: ";
	getline(cin, word);
	cout << "Input: " << word << endl;
}

void main() {
	string word;
	inputWord(word);
	cout << "Reverse Word: "; 
	printReverseWord(word);
}