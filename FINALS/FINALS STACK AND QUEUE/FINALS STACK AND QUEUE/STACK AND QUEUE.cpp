#include<iostream>
#include<string>
#include"Stack.h"
#include"Queue.h"

using namespace std;

int inputArraySize() {
	int number; 
	cout << "Enter the size of the Array: ";
	cin >> number; 
	if (number <= 0) {
		inputArraySize();
	}
	return number;
}
void displayStackArray(Stack<int>& stackArray) {
	cout << "Stack: ";
	for (int i = 0; i < stackArray.getSize(); i++) {
		cout << stackArray[i] << ", ";
	}
	cout << endl; 
}
void displayQueueArray(Queue<int>& queueArray) {
	cout << "Queue: ";
	for (int i = 0; i < queueArray.getSize()-0; i++) {
		cout << queueArray[i] << ", ";
	}
	cout << endl; 
}

void enterNumberArray(Stack<int>& stackArray, Queue<int>& queueArray) {
	int number; 
	cout << "Enter number: "; 
	cin >> number;
	stackArray.pushFront(number);
	queueArray.pushBack(number);
}

void displayTopElements(Stack<int>& stackArray, Queue<int>& queueArray) {
	cout << "Top Elements of sets" << endl;
	cout << "Queue: " << queueArray.top() << endl;
	cout << "Stack: " << stackArray.top() << endl; 
}

void removeAllElements(Stack<int>& stackArray, Queue<int>& queueArray) {
	for (int i = 0; i < stackArray.getSize(); i++) {
		stackArray.pop();
	}
	for (int i = 0; i < queueArray.getSize(); i++) {
		queueArray.pop();
	}
}
void userMainMenu(Stack<int>& stackArray, Queue<int>& queueArray) {
	int userChoice;
	cout << "Enter next action: " << endl; 
	cout << "[1] = Push Elements" << endl; 
	cout << "[2] = Pop Elements " << endl; 
	cout << "[3] = Print all elements in the set then empty set" << endl; 
	cin >> userChoice;

	if (userChoice == 1) {
		enterNumberArray(stackArray, queueArray);
		displayTopElements(stackArray, queueArray);
	}
	else if (userChoice == 2) {
		cout << "The top elements of the sets have been removed" << endl;
		stackArray.pop();
		queueArray.pop();
	}
	else if (userChoice == 3) {
		displayStackArray(stackArray); 
		displayQueueArray(queueArray);
		removeAllElements(stackArray, queueArray);
	}
	else if (userChoice == 4) {
		displayStackArray(stackArray);
		displayQueueArray(queueArray);
	}
	else if (userChoice > 3 || userChoice < 1) {
		userMainMenu(stackArray, queueArray);
	}
	system("pause");
	system("cls");
}

int main() {

	int sizeOfArray = inputArraySize();
	Stack<int> stack(sizeOfArray);
	Queue<int> queue(sizeOfArray);
	
	while (true) {
		userMainMenu(stack, queue);
	}
	system("pause");
}